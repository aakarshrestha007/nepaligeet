package com.example.aakarshrestha.mykotlinapplication.UI

import android.app.FragmentTransaction
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.app.Fragment
import android.app.FragmentManager
import android.os.Parcelable
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.NetworkApi.NetworkApi
import com.example.aakarshrestha.mykotlinapplication.UI.adapter.RecentlyPlayedTrackAdapter
import com.example.aakarshrestha.mykotlinapplication.UI.adapter.TrackAdapter
import com.example.aakarshrestha.mykotlinapplication.UI.model.Song
import com.example.aakarshrestha.mykotlinapplication.UI.urls.NetworkApiUrl
import com.example.aakarshrestha.mykotlinapplication.UI.utils.BottomNavigationViewHelper
import com.example.aakarshrestha.mykotlinapplication.databinding.ActivityParentBinding
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import android.content.Intent



class ParentActivity : AppCompatActivity() {

    val listOfTracks = arrayListOf<Song>()
    val listOfRecentlyPlayedTracks = arrayListOf<Song>()
    val listOfNewReleaseTracks = arrayListOf<Song>()

    var loadedListOfTracks = arrayListOf<Song>()
    var loadedListOfRecentlyPlayedTracks = arrayListOf<Song>()
    var loadedListOfNewReleaseTracks = arrayListOf<Song>()

    lateinit var binding: ActivityParentBinding
    lateinit var menu: Menu
    val tag: String = "MTAG"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_parent)
        BottomNavigationViewHelper.disableShiftMode(binding.mbottomnavigation)

        launch(UI) {

            bg { getTracks() }

//            bg { getRecentlyPlayedTracks() }
//
//            bg { getNewReleaseTracks() }
        }

        menu = binding.mbottomnavigation.menu
//        selectFragment(menu.getItem(0))

        binding.mbottomnavigation.setOnNavigationItemSelectedListener { item ->

            selectFragment(item)
            false
        }

    }

    override fun onBackPressed() {

        //Exit When Back and Set no History
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP//***Change Here***
        startActivity(intent)
        finish()
        System.exit(0)
    }

    private fun  selectFragment(item: MenuItem) {

        item.isChecked = true

        when(item.itemId) {

            R.id.menu_home -> {
                val bundle: Bundle = Bundle()

                try {
                    bundle.putParcelableArrayList("homeArrayList", loadedListOfTracks)
                    Log.d("mysizehai1", "${loadedListOfTracks.size} size")
                    getHomeFragment(HomeFragment(), bundle)
                } catch (cce: ClassCastException) {
                    Log.e("parentActivityError", cce.message)
                }
            }

            R.id.menu_browser -> getFragment(BrowserFragment())
            R.id.menu_search -> getFragment(SearchFragment())
            R.id.menu_library -> getFragment(YourLibraryFragment())
        }

    }

    private fun  getHomeFragment(fragment: Fragment, bundle: Bundle) {

        if(false) {
            return
        }

        val fragmentManager: FragmentManager? = fragmentManager
        val fragmentTransaction: FragmentTransaction? = fragmentManager?.beginTransaction()
        fragment.arguments = bundle
        fragmentTransaction?.replace(R.id.container, fragment)?.commit()
    }

    private fun  getFragment(fragment: Fragment) {

        if(false) {
            return
        }

        val fragmentManager: FragmentManager? = fragmentManager
        val fragmentTransaction: FragmentTransaction? = fragmentManager?.beginTransaction()
        fragmentTransaction?.replace(R.id.container, fragment)?.commit()
    }

    //

    fun getTracks() {

        val m_track = async(UI) {

            val call = NetworkApi.getDataOnCall(NetworkApiUrl.getNetworkApiUri())

            val data = bg {

                call.execute().body()!!.string()

            }

            val trackData = data.await()
            Log.d("trackdata", trackData.toString())

            try {

                val getTrackData = bg {

                    val jsonObject = JSONObject(trackData)

                    val jsonTracks: String = jsonObject.getString("tracks")
                    Log.d("trackdata1", jsonTracks)

                    val jsonArray: JSONArray = jsonObject.getJSONArray("tracks")

                    for(i in 0 until jsonArray.length()) {

                        Log.d("trackdata2", jsonArray.get(i).toString())
                        val jsonArrayString = jsonArray.get(i).toString()
                        Log.d("trackdata3", jsonArrayString)

                        val mjsonObj = JSONObject(jsonArrayString)
                        val artistName = mjsonObj.getString("artistName")
                        val albumName = mjsonObj.getString("albumName")
                        val previewUrl = mjsonObj.getString("previewURL")
                        Log.d("trackdata4", "$artistName $albumName")

                        val song = Song()
                        with(song) {
                            artistname = artistName
                            albumname = albumName
                            previewurl = previewUrl
                        }

                        listOfTracks.add(song)
                    }

                    listOfTracks
                }

                val tracksList = getTrackData.await()
                loadedListOfTracks = tracksList
                /*
                    now select the first menu item
                 */
                selectFragment(menu.getItem(0))
                Log.d("mysizehai1", "${loadedListOfTracks.size} size")
                Log.d("trackdata5", "${tracksList.size} tracks")

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    fun getRecentlyPlayedTracks() {

        val m_track = async(UI) {

            val call = NetworkApi.getDataOnCall(NetworkApiUrl.getNetworkApiUri())

            val data = bg {

                call.execute().body()!!.string()

            }

            val trackData = data.await()
            Log.d("trackdata", trackData.toString())

            try {

                val getTrackData = bg {

                    val jsonObject = JSONObject(trackData)

                    val jsonTracks: String = jsonObject.getString("tracks")
                    Log.d("trackdata1", jsonTracks)

                    val jsonArray: JSONArray = jsonObject.getJSONArray("tracks")

                    for(i in 0 until jsonArray.length()) {

                        Log.d("trackdata2", jsonArray.get(i).toString())
                        val jsonArrayString = jsonArray.get(i).toString()
                        Log.d("trackdata3", jsonArrayString)

                        val mjsonObj = JSONObject(jsonArrayString)
                        val artistName = mjsonObj.getString("artistName")
                        val albumName = mjsonObj.getString("albumName")
                        val previewUrl = mjsonObj.getString("previewURL")
                        Log.d("trackdata4", "$artistName $albumName")

                        val song = Song()
                        with(song) {
                            artistname = artistName
                            albumname = albumName
                            previewurl = previewUrl
                        }

                        listOfRecentlyPlayedTracks.add(song)
                    }

                    listOfRecentlyPlayedTracks
                }

                val tracksList = getTrackData.await()
                loadedListOfRecentlyPlayedTracks = tracksList
                Log.d("trackdata5", "${tracksList.size} tracks")

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    fun getNewReleaseTracks() {

        val m_track = async(UI) {

            val call = NetworkApi.getDataOnCall(NetworkApiUrl.getNetworkApiUri())

            val data = bg {

                call.execute().body()!!.string()

            }

            val trackData = data.await()
            Log.d("trackdata", trackData.toString())

            try {

                val getTrackData = bg {

                    val jsonObject = JSONObject(trackData)

                    val jsonTracks: String = jsonObject.getString("tracks")
                    Log.d("trackdata1", jsonTracks)

                    val jsonArray: JSONArray = jsonObject.getJSONArray("tracks")

                    for(i in 0 until jsonArray.length()) {

                        Log.d("trackdata2", jsonArray.get(i).toString())
                        val jsonArrayString = jsonArray.get(i).toString()
                        Log.d("trackdata3", jsonArrayString)

                        val mjsonObj = JSONObject(jsonArrayString)
                        val artistName = mjsonObj.getString("artistName")
                        val albumName = mjsonObj.getString("albumName")
                        val previewUrl = mjsonObj.getString("previewURL")
                        Log.d("trackdata4", "$artistName $albumName")

                        val song = Song()
                        with(song) {
                            artistname = artistName
                            albumname = albumName
                            previewurl = previewUrl
                        }

                        listOfNewReleaseTracks.add(song)
                    }

                    listOfNewReleaseTracks
                }

                val tracksList = getTrackData.await()
                loadedListOfNewReleaseTracks = tracksList
                Log.d("trackdata5", "${tracksList.size} tracks")

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }
}
