package com.example.aakarshrestha.mykotlinapplication.UI

import android.databinding.DataBindingUtil
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.NetworkApi.NetworkApi
import com.example.aakarshrestha.mykotlinapplication.UI.urls.NetworkApiUrl
import com.example.aakarshrestha.mykotlinapplication.databinding.ActivityForgotPasswordBinding
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.regex.Pattern

class ForgotPasswordActivity : AppCompatActivity() {

    lateinit var binding: ActivityForgotPasswordBinding
    val VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)

        val typeFace: Typeface = Typeface.createFromAsset(assets, "fonts/lobstertwobold.ttf")
        binding.appname.typeface = typeFace
        binding.nameOfApp.typeface = typeFace

        binding.appbarlayout.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (Math.abs(verticalOffset) == appBarLayout!!.totalScrollRange) {
                // Collapsed
                binding.nameOfApp.visibility = View.VISIBLE

            } else if (verticalOffset == 0) {
                // Expanded
                binding.nameOfApp.visibility = View.INVISIBLE

            }
        }

        binding.forgotEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.length!! > 0 && s.toString().trim().isNotEmpty() && s.toString().trim() != "") {

                    if (validate(s.toString())) {
                        binding.send.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
                        binding.send.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
                        binding.send.isEnabled = true
                    } else {
                        binding.send.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                        binding.send.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                        binding.send.isEnabled = false
                    }

                } else {
                    binding.send.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                    binding.send.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                    binding.send.isEnabled = false
                }

            }
        })

        binding.send.setOnClickListener({

//            val value = NetworkApi.postForgotPasswordData(binding.emailAddress.text.toString().trim())
//            Log.d("studyThisNow", value + " haha")

            val email = binding.forgotEmail.text.toString().trim()
            postForgotPasswordData(email)


//            binding.forgotPasswordLayout.visibility = View.GONE
//            binding.resetPasswordLayout.visibility = View.VISIBLE


        })

    }

    fun validate(emailStr: String): Boolean {
        val matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr)
        return matcher.find()
    }

    fun postForgotPasswordData(email: String) {

        try {
            val call: Call = NetworkApi.getDataOnCall(NetworkApiUrl.forgotPasswordUrl()+email)
            call.enqueue(object : Callback {
                override fun onFailure(call: Call?, e: IOException?) {

                }

                override fun onResponse(call: Call?, response: Response?) {

                    if (response?.isSuccessful!!) {

                        Log.d("studyThisNow", response.body()?.string()!! + " haha")


                    } else {

                        Log.d("studyThisNow", response.body()?.string()!! + " NO haha")
                    }
                }
            })

        } catch (ioe: IOException) {
            Log.e("ioe", ioe.message)
        }
    }
}
