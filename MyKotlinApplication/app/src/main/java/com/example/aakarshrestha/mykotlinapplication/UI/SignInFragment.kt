package com.example.aakarshrestha.mykotlinapplication.UI


import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.NetworkApi.NetworkApi
import com.example.aakarshrestha.mykotlinapplication.databinding.FragmentSignInBinding
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class SignInFragment : Fragment() {

    lateinit var binding: FragmentSignInBinding

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)

        binding.username.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.length!! > 1 && !binding.password.text.isEmpty() && binding.password.text.length > 3) {

                    binding.loginButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
                    binding.loginButton.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
                    binding.loginButton.isEnabled = true

                } else {

                    binding.loginButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                    binding.loginButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                    binding.loginButton.isEnabled = false
                }

            }

        })

        binding.password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.length!! > 3 && !binding.username.text.isEmpty() && binding.username.text.length > 1) {

                    binding.loginButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
                    binding.loginButton.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
                    binding.loginButton.isEnabled = true

                } else {

                    binding.loginButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                    binding.loginButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                    binding.loginButton.isEnabled = false

                }

            }
        })

        binding.loginButton.setOnClickListener {

            binding.progressbar.visibility = View.VISIBLE
            binding.loginButton.visibility = View.GONE

            async(UI) {

                val authenticateUserInBackground = bg {

                    NetworkApi.loginToApp(binding.username.text.toString().trim(), binding.password.text.toString())

                }

                val userAuthValue = authenticateUserInBackground.await()

                if (userAuthValue.equals("success", true)) {

                    binding.progressbar.visibility = View.GONE
                    binding.loginButton.visibility = View.GONE

                    val intent = Intent(binding.root.context, ParentActivity::class.java)
                    intent.putExtra("email", binding.username.text.toString().trim())
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    binding.root.context.startActivity(intent)

                    activity.finish()


                } else {
                    binding.progressbar.visibility = View.GONE
                    binding.loginButton.visibility = View.VISIBLE

                    val mySnackbar = Snackbar.make(binding.coordinatorLayout,
                            "Email or password does not match!", Snackbar.LENGTH_SHORT)
                    mySnackbar.show()
                }


            }

        }

        binding.forgotPassword.setOnClickListener({

            val intent = Intent(binding.root.context, ForgotPasswordActivity::class.java)
            startActivity(intent)

        })

        return binding.root
    }

}
