package com.example.aakarshrestha.mykotlinapplication.UI

import android.databinding.DataBindingUtil
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.View
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.databinding.ActivitySignUpSignInBinding

class SignUpSignInActivity : AppCompatActivity() {

    lateinit var binding: ActivitySignUpSignInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up_sign_in)

        val typeFace: Typeface = Typeface.createFromAsset(assets, "fonts/lobstertwobold.ttf")
        binding.appname.typeface = typeFace
        binding.nameOfApp.typeface = typeFace

        setupViewPager()
        binding.tabs.setupWithViewPager(binding.viewpager)

        binding.appbarlayout.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (Math.abs(verticalOffset) == appBarLayout!!.totalScrollRange) {
                // Collapsed
                binding.nameOfApp.visibility = View.VISIBLE

            } else if (verticalOffset == 0) {
                // Expanded
                binding.nameOfApp.visibility = View.INVISIBLE

            }
        }

    }

    fun setupViewPager() {

        val viewPagerAdapter: ViewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addFragment(SignInFragment(), "Log In")
        viewPagerAdapter.addFragment(SignUpFragment(), "Sign Up")
        binding.viewpager.adapter = viewPagerAdapter

    }

    class ViewPagerAdapter (fm: FragmentManager?) : FragmentPagerAdapter(fm) {

        var mFragmentList = mutableListOf<Fragment>()
        var mFragmentTitleList = mutableListOf<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }

    }
}
