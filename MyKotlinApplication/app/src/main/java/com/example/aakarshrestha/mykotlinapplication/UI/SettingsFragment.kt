package com.example.aakarshrestha.mykotlinapplication.UI


import android.os.Bundle
import android.app.Fragment
import android.app.FragmentManager
import android.app.FragmentTransaction
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.SignIn.SignInWithGoogle
import com.example.aakarshrestha.mykotlinapplication.databinding.FragmentSettingsBinding
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient

/**
 * A simple [Fragment] subclass.
 */
class SettingsFragment : Fragment() {

    lateinit var binding: FragmentSettingsBinding
    lateinit var googleApiClient: GoogleApiClient
    lateinit var googleSignInClient: GoogleSignInClient
    lateinit var gso: GoogleSignInOptions

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)

        binding.settingsToolbar.setOnClickListener({

            val fragment = YourLibraryFragment()
            val fragmentManager: FragmentManager? = fragmentManager
            val fragmentTransaction: FragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.replace(R.id.container, fragment)?.commit()

        })

        googleApiClient = SignInWithGoogle.getGoogleApiClient(binding.root.context)
        gso = SignInWithGoogle.getGoogleSignInOptions(binding.root.context)
        googleSignInClient = SignInWithGoogle.getGoogleSignInClient(gso, binding.root.context)

        binding.logoutLayout.setOnClickListener({
            signOut()
        })

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()

        if(googleApiClient.isConnected) {
            googleApiClient.stopAutoManage(binding.root.context as FragmentActivity)
            googleApiClient.disconnect()
        }

    }

    override fun onStart() {
        super.onStart()
        googleApiClient.connect()
    }

    override fun onStop() {
        super.onStop()
        googleApiClient.disconnect()
    }

    fun signOut() {

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback { status ->
            if (status.isSuccess) {
                gotoLogin()
            } else {
                Log.e("errorYo", "Error found!")
            }
        }

        revokeAccess()

    }

    private fun revokeAccess() {

    }

    fun gotoLogin() {

        val intent: Intent = Intent(binding.root.context, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        binding.root.context.startActivity(intent)

    }

}
