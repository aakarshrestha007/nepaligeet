package com.example.aakarshrestha.mykotlinapplication.UI.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.aakarshrestha.mykotlinapplication.BR
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.model.Song
import com.example.aakarshrestha.mykotlinapplication.databinding.ListItemTrackBinding

/**
 * Created by aakarshrestha on 3/3/18.
 */
class ShufflePlayTrackAdapter (val context: Context, val listOfSongs: MutableList<Song>) : RecyclerView.Adapter<ShufflePlayTrackAdapter.BindingHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder {
        val itemBinding = DataBindingUtil.inflate<ListItemTrackBinding>(LayoutInflater.from(parent.context), R.layout.list_item_track, parent, false)
        return BindingHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: BindingHolder, position: Int) {

        val msong = listOfSongs[position]
        val itemBinding = holder.binding
        itemBinding.setVariable(BR.track, msong)
        itemBinding.executePendingBindings()

        itemBinding.artistnameID.text = msong.artistname.toString()
        itemBinding.songnameID.text = msong.albumname.toString()

    }

    override fun getItemCount(): Int {
        return listOfSongs.size
    }

    class BindingHolder (val binding: ListItemTrackBinding) : RecyclerView.ViewHolder(binding.root)

}