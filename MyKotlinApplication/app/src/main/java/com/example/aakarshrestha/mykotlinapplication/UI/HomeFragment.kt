package com.example.aakarshrestha.mykotlinapplication.UI

import android.os.Bundle
import android.app.Fragment
import android.app.FragmentManager
import android.app.FragmentTransaction
import android.databinding.DataBindingUtil
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.NetworkApi.NetworkApi
import com.example.aakarshrestha.mykotlinapplication.UI.adapter.RecentlyPlayedTrackAdapter
import com.example.aakarshrestha.mykotlinapplication.UI.adapter.TrackAdapter
import com.example.aakarshrestha.mykotlinapplication.UI.model.Song
import com.example.aakarshrestha.mykotlinapplication.UI.urls.NetworkApiUrl
import com.example.aakarshrestha.mykotlinapplication.databinding.FragmentHomeBinding
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class HomeFragment : Fragment() {

    lateinit var binding: FragmentHomeBinding

    var listOfTracks = arrayListOf<Song>()
    var linearLayout: LinearLayoutManager? = null
    var linearLayoutForRecentlyPlayedTrack: LinearLayoutManager? = null
    var linearLayoutForNewReleaseTrack: LinearLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            listOfTracks.addAll(arguments.getParcelableArrayList<Song>("homeArrayList"))
            Log.d("mysizehai", "${listOfTracks.size} size")
        }

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(LayoutInflater.from(container?.context), R.layout.fragment_home, container, false)

        linearLayout = LinearLayoutManager(binding.root.context.applicationContext, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerview.layoutManager = linearLayout

        linearLayoutForRecentlyPlayedTrack = LinearLayoutManager(binding.root.context.applicationContext, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerviewRecentlyplayed.layoutManager = linearLayoutForRecentlyPlayedTrack

        linearLayoutForNewReleaseTrack = LinearLayoutManager(binding.root.context.applicationContext, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerviewNewreleases.layoutManager = linearLayoutForNewReleaseTrack

        val trackAdapter: TrackAdapter = TrackAdapter(binding.root.context, listOfTracks)
        binding.recyclerview.adapter = trackAdapter
        trackAdapter.notifyDataSetChanged()

        val recentlyPlayedtrackAdapter = RecentlyPlayedTrackAdapter(binding.root.context, listOfTracks)
        binding.recyclerviewRecentlyplayed.adapter = recentlyPlayedtrackAdapter
        recentlyPlayedtrackAdapter.notifyDataSetChanged()

        val newReleasetrackAdapter = RecentlyPlayedTrackAdapter(binding.root.context, listOfTracks)
        binding.recyclerviewNewreleases.adapter = newReleasetrackAdapter
        newReleasetrackAdapter.notifyDataSetChanged()

        return binding.root
    }

}
