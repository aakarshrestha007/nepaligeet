package com.example.aakarshrestha.mykotlinapplication.UI

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v4.view.ViewPager.OnPageChangeListener
import android.view.View
import android.widget.Toast
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.adapter.SliderTrackAdapter
import com.example.aakarshrestha.mykotlinapplication.databinding.ActivityDetailTrackBinding
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.android.UI

class DetailTrackActivity : AppCompatActivity() {

    /// For testing only

    var artistNameArray = arrayListOf(

            "Cmc Beats",
            "Bebe Rexha",
            "Ed Sheeran",
            "Drake",
            "Bruno Mars",
            "Scary Hours",
            "Berry White",
            "Migos"


    )
    ////

    lateinit var binding: ActivityDetailTrackBinding
    var sliderTrackAdapter: SliderTrackAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.setContentView(this, R.layout.activity_detail_track)

        sliderTrackAdapter = SliderTrackAdapter(binding.root.context)
        binding.viewpagerID.adapter = sliderTrackAdapter

        binding.viewpagerID.addOnPageChangeListener(viewPagerListener)

        binding.playID.setOnClickListener {

            binding.playID.visibility = View.GONE
            binding.pauseID.visibility = View.VISIBLE

        }

        binding.pauseID.setOnClickListener {

            binding.pauseID.visibility = View.GONE
            binding.playID.visibility = View.VISIBLE

        }

        binding.menuActivityDetailNavigationLayout.setOnClickListener {

            finish()

        }
    }

    override fun onBackPressed() {

        finish()

    }

    var viewPagerListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

        }

        override fun onPageSelected(position: Int) {
            launch(UI) {
                Toast.makeText(binding.root.context, artistNameArray[position], Toast.LENGTH_SHORT).show()
                binding.pauseID.visibility = View.VISIBLE
                binding.playID.visibility = View.GONE
            }
        }

    }
}
