package com.example.aakarshrestha.mykotlinapplication.UI


import android.app.Activity
import android.os.Bundle
import android.app.Fragment
import android.app.FragmentManager
import android.app.FragmentTransaction
import android.content.Intent
import android.databinding.DataBindingUtil
import android.media.MediaPlayer
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.adapter.ShufflePlayTrackAdapter
import com.example.aakarshrestha.mykotlinapplication.UI.model.Song
import com.example.aakarshrestha.mykotlinapplication.databinding.FragmentTrackBinding
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import org.jetbrains.anko.coroutines.experimental.bg
import java.io.IOException
import java.util.*

class TrackFragment : Fragment(), MediaPlayer.OnCompletionListener {

    lateinit var binding: FragmentTrackBinding
    var listOfSongs: MutableList<Song>? = null
    var linearLayout: LinearLayoutManager? = null
    var paused: Int = 0

    lateinit var mediaPlayer: MediaPlayer
    var currentSongIndex = 0
    var shuffle: String = ""
    var isPlayClicked: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.trackAppBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener{
            override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {

                if (Math.abs(verticalOffset) == appBarLayout!!.totalScrollRange) {
                    // Collapsed
                    binding.artistnameInToolbarText.visibility = View.VISIBLE

                } else if (verticalOffset == 0) {
                    // Expanded
                    binding.artistnameInToolbarText.visibility = View.INVISIBLE

                }

            }

        })

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(LayoutInflater.from(container?.context), R.layout.fragment_track, container, false)

        linearLayout = LinearLayoutManager(binding.root.context)
        binding.recyclerviewShuffleplay.layoutManager = linearLayout
        binding.recyclerviewShuffleplay.isNestedScrollingEnabled = false

        binding.backarrow.setOnClickListener {

            val fragmentManager: FragmentManager = fragmentManager
            fragmentManager.popBackStackImmediate()

        }

        val i_artistname = arguments.getString("t_artistname")
        val i_albumname = arguments.getString("t_albumname")
        val i_previewurl = arguments.getString("t_previewurl")

        binding.artistnameInToolbarText.text = i_artistname
        binding.artistnameRelID.text = i_artistname

        val song = Song()
        with(song) {
            artistname = i_artistname
            albumname = i_albumname
            previewurl = i_previewurl
        }
        listOfSongs = mutableListOf()
        listOfSongs?.add(song)

        val song1 = Song()
        with(song1) {
            artistname = i_artistname + "1"
            albumname = i_albumname
            previewurl = "http://listen.vo.llnwd.net/g3/9/7/9/6/7/1384076979.mp3"
        }
        listOfSongs?.add(song1)

        val song2 = Song()
        with(song2) {
            artistname = i_artistname + "2"
            albumname = i_albumname
            previewurl = "http://listen.vo.llnwd.net/g3/3/4/7/4/9/1388194743.mp3"
        }
        listOfSongs?.add(song2)

        val song3 = Song()
        with(song3) {
            artistname = i_artistname + "3"
            albumname = i_albumname
            previewurl = "http://listen.vo.llnwd.net/g3/0/4/6/9/9/1358499640.mp3"
        }
        listOfSongs?.add(song3)

        val song4 = Song()
        with(song4) {
            artistname = i_artistname + "4"
            albumname = i_albumname
            previewurl = "http://listen.vo.llnwd.net/g3/5/3/8/7/0/1373907835.mp3"
        }
        listOfSongs?.add(song4)
        listOfSongs?.add(song4)
        listOfSongs?.add(song4)
        listOfSongs?.add(song4)
        listOfSongs?.add(song4)
        listOfSongs?.add(song4)
        listOfSongs?.add(song4)
        listOfSongs?.add(song4)

        launch(UI) {

            val shuffleTrackAdapter = ShufflePlayTrackAdapter(binding.root.context.applicationContext, listOfSongs!!)
            binding.recyclerviewShuffleplay.adapter = shuffleTrackAdapter
            shuffleTrackAdapter.notifyDataSetChanged()

        }

        mediaPlayer = MediaPlayer()
        mediaPlayer.setOnCompletionListener(this)

        binding.playview.setOnClickListener {

            launch(UI) {

                binding.playview.visibility = View.GONE
                binding.pauseview.visibility = View.VISIBLE

                if (!isPlayClicked) {
                    val random = Random()
                    currentSongIndex = random.nextInt((listOfSongs!!.size - 1) - 0 + 1) + 0
                    playTheTrack(currentSongIndex)
                    isPlayClicked = true
                    shuffle = "noshuffle"

                    binding.mSongName.text = listOfSongs!![currentSongIndex].albumname
                    binding.mArtistName.text = listOfSongs!![currentSongIndex].artistname
                }

                mediaPlayer.start()

                Log.d("onpaused", mediaPlayer.duration.toString())

            }

        }

        binding.shuffleplay.setOnClickListener {

            binding.relLayoutbottom.visibility = View.VISIBLE
            binding.seekbar.visibility = View.VISIBLE

            launch(UI) {

                binding.playview.visibility = View.GONE
                binding.pauseview.visibility = View.VISIBLE

                if (!isPlayClicked) {
                    playTheTrack(getRandomValue())
                    isPlayClicked = true
                    shuffle = "shuffle"

                    binding.mSongName.text = listOfSongs!![currentSongIndex].albumname
                    binding.mArtistName.text = listOfSongs!![currentSongIndex].artistname
                }

                mediaPlayer.start()

                Log.d("onpaused", mediaPlayer.duration.toString())

            }

        }

        binding.pauseview.setOnClickListener {

            binding.playview.visibility = View.VISIBLE
            binding.pauseview.visibility = View.GONE

            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
                paused = mediaPlayer.currentPosition / 1000
            }

        }

        binding.relLayoutbottom.setOnClickListener {

            val intent: Intent = Intent(binding.root.context, DetailTrackActivity::class.java)
            binding.root.context.startActivity(intent)

        }

        return binding.root
    }

    fun playTheTrack(songIndex: Int) {

        launch(UI) {

            try {

                mediaPlayer.reset()
                mediaPlayer.setDataSource(listOfSongs!![songIndex].previewurl.toString())
                mediaPlayer.prepare()
                mediaPlayer.start()

                binding.playview.visibility = View.GONE
                binding.pauseview.visibility = View.VISIBLE

                binding.seekbar.max = mediaPlayer.duration / 1000

                while (mediaPlayer.duration / 1000 != mediaPlayer.currentPosition / 1000) {
                    delay(1000)
                    binding.seekbar.progress = mediaPlayer.currentPosition / 1000
                    Log.d("onpaused1", (mediaPlayer.currentPosition / 1000).toString())
                }

            } catch (iae: IllegalArgumentException) {
                iae.printStackTrace()
            } catch (ise: IllegalStateException) {
                ise.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }

    override fun onCompletion(mp: MediaPlayer?) {

        launch(UI) {
            binding.playview.visibility = View.VISIBLE
            binding.pauseview.visibility = View.GONE
        }

        if (shuffle.equals("shuffle", true)) {

            launch(UI) {
                delay(2000)

                currentSongIndex = getRandomValue()
                playTheTrack(currentSongIndex)
                binding.mSongName.text = listOfSongs!![currentSongIndex].albumname
                binding.mArtistName.text = listOfSongs!![currentSongIndex].artistname
                Log.d("cupcupcup", currentSongIndex.toString())
            }

        } else {

            if (currentSongIndex < (listOfSongs!!.size - 1)) {

                launch(UI) {
                    delay(2000)

                    playTheTrack(currentSongIndex)
                    binding.mSongName.text = listOfSongs!![currentSongIndex].albumname
                    binding.mArtistName.text = listOfSongs!![currentSongIndex].artistname
                    currentSongIndex++
                    Log.d("cupcupcup1", "next $currentSongIndex")
                }
            }

        }

    }

    fun getRandomValue(): Int {

        var numvalue: Int = 0

        if (listOfSongs!!.size > 0) {
            val random = Random()
            numvalue = random.nextInt((listOfSongs!!.size - 1) - 0 + 1) + 0
        }

        return numvalue

    }

}
