package com.example.aakarshrestha.mykotlinapplication.UI.model

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by aakarshrestha on 3/2/18.
 */
class Song () : BaseObservable(), Parcelable {

    @Bindable
    var artistname: String? = null
    @Bindable
    var albumname: String? = null
    @Bindable
    var previewurl: String? = null

    constructor(`in`: Parcel) : this() {
        artistname = `in`.readString()
        albumname = `in`.readString()
        previewurl = `in`.readString()
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(artistname)
        dest?.writeString(albumname)
        dest?.writeString(previewurl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {

        val CREATOR: Parcelable.Creator<Song> = object : Parcelable.Creator<Song> {
            override fun createFromParcel(`in`: Parcel): Song {
                return Song(`in`)
            }

            override fun newArray(size: Int): Array<Song?> {
                return arrayOfNulls(size)
            }
        }
    }

}