package com.example.aakarshrestha.mykotlinapplication.UI.urls

/**
 * Created by aakarshrestha on 3/2/18.
 */
class NetworkApiUrl {

    companion object {
        fun getNetworkApiUri(): String =
                "https://api.napster.com/v2.1/tracks/top?apikey=ZTk2YjY4MjMtMDAzYy00MTg4LWE2MjYtZDIzNjJmMmM0YTdm"

        val baseUrl = "http://nepaligeet.azurewebsites.net/"
        fun createAccount(): String = "${baseUrl}api/Accounts"
        fun loginToAccount(): String = "${baseUrl}api/Auth/login"
        fun verifyGoogleTokenUrl(): String = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="
        fun forgotPasswordUrl(): String = "http://nepaligeet.azurewebsites.net/api/Accounts/ForogtPassword?Email="
        fun oauth2TokenUrl(): String = "https://www.googleapis.com/oauth2/v4/token"
        fun getAccessToken(): String = "https://accounts.google.com/o/oauth2/token"
        fun externalAuthGoogle(): String = "${baseUrl}api/ExternalAuth/Google"
    }

}