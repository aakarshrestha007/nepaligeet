package com.example.aakarshrestha.mykotlinapplication.UI

import android.content.ActivityNotFoundException
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.NetworkApi.NetworkApi
import com.example.aakarshrestha.mykotlinapplication.UI.SignIn.SignInWithGoogle
import com.example.aakarshrestha.mykotlinapplication.UI.urls.NetworkApiUrl
import com.example.aakarshrestha.mykotlinapplication.databinding.ActivityLoginBinding
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.experimental.launch
import okhttp3.*

import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import kotlinx.coroutines.experimental.android.UI


class LoginActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    lateinit var loginActivity: LoginActivity

    lateinit var binding: ActivityLoginBinding
    lateinit var googleSignInClient : GoogleSignInClient
    val GOOGLE_REQUEST_CODE: Int = 12

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        binding.progressBar.visibility = View.GONE

        loginActivity = this

        val typeFace: Typeface = Typeface.createFromAsset(assets, "fonts/lobstertwobold.ttf")
        binding.appname.typeface = typeFace

        binding.connectWithFacebook.setOnClickListener {

            val intent: Intent = Intent(binding.root.context, ParentActivity::class.java)

            try {
                startActivity(intent)
            } catch (anfe: ActivityNotFoundException) {
                anfe.message
            }

        }

        /**
         * GoogleSignIn setup is done in "getGoogleApiClient" method in SignInWithGoogle class
         */

        SignInWithGoogle.getGoogleApiClient(binding.root.context)
        googleSignInClient = SignInWithGoogle.getGoogleSignInClient(SignInWithGoogle.getGoogleSignInOptions(binding.root.context), binding.root.context)

        binding.connectWithGoogle.setOnClickListener {
            googleSignIn()
        }

        /**
         * Sign up and/or Sign in
         */

        binding.signupsigninlayout.setOnClickListener {

            val intent = Intent(binding.root.context, SignUpSignInActivity::class.java)
            startActivity(intent)

        }
    }

    override fun onStart() {
        super.onStart()

        val account = GoogleSignIn.getLastSignedInAccount(this)

        if (account != null) {
            logInWithVerification(account)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GOOGLE_REQUEST_CODE) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleDataFromGoogleSignIn(task)
        }

    }

    /**
     * Google authentication funtions
     */

    fun googleSignIn() {
        val intent: Intent = googleSignInClient.signInIntent
        startActivityForResult(intent, GOOGLE_REQUEST_CODE)
    }

    fun logInWithVerification(account: GoogleSignInAccount) {

        val intent = Intent(binding.root.context, ParentActivity::class.java)
        intent.putExtra("email", account.email)
        intent.putExtra("name", account.displayName)
        intent.putExtra("serverAuthCode", account.serverAuthCode)
        intent.putExtra("idToken", account.idToken)
        startActivity(intent)

    }

    fun handleDataFromGoogleSignIn(task: Task<GoogleSignInAccount>) {

        try {
            val account = task.getResult(ApiException::class.java)
            launch(UI) {
                Log.d("showToken", account.idToken + "\n\n" + account.serverAuthCode)
                getGoogleAccessToken(account.serverAuthCode!!, account)
            }
        } catch (ae : ApiException) {
            ae.message
        }

    }

    fun updateUI(account: GoogleSignInAccount) {

        launch(UI) {
            verifyThisUserIsGoogleAuthenticated(account.idToken!!, account)
        }

    }

    fun verifyThisUserIsGoogleAuthenticated(token: String, account: GoogleSignInAccount) {

        val url = NetworkApiUrl.verifyGoogleTokenUrl()
        val call: Call = NetworkApi.getDataOnCall(url + token)
        call.enqueue(object : Callback {
            override fun onFailure(call: Call?, e: IOException?) {

            }

            override fun onResponse(call: Call?, response: Response?) {

                if (response?.isSuccessful!!) {

                    try {

                        var mResponse = response.body()?.string()

                        val jsonObject = JSONObject(mResponse)
                        val sub = jsonObject.getString("aud")
                        val email_verified = jsonObject.getString("email_verified")

                        if (sub.equals(resources.getString(R.string.server_client_id), true) && email_verified.equals("true", true)) {

                            val intent = Intent(binding.root.context, ParentActivity::class.java)
                            intent.putExtra("email", account.email)
                            intent.putExtra("name", account.displayName)
                            intent.putExtra("serverAuthCode", account.serverAuthCode)
                            intent.putExtra("idToken", account.idToken)
                            startActivity(intent)

                        } else {
                            Toast.makeText(binding.root.context, "Cannot authenticate you in Google!", Toast.LENGTH_SHORT).show()
                        }

                    } catch (je: JSONException) {
                        je.message
                    }

                } else {
                    runOnUiThread({
                        Toast.makeText(binding.root.context, "Network Issue, please try again!", Toast.LENGTH_SHORT).show()
                    })
                }

            }
        })

    }

    fun getGoogleAccessToken(aServerAuthCode: String, account: GoogleSignInAccount) {

        var responseBodyValue: String

        val okhttp = NetworkApi.getOkhttp()
        val requestBody: RequestBody = FormBody.Builder()
                .add("grant_type", "authorization_code")
                .add("client_id", resources.getString(R.string.server_client_id))
                .add("client_secret", "gwvC4IS4GOrNfMyWtYN1rE4n")
                .add("code", aServerAuthCode)
                .build()

        val request = Request.Builder()
                .url(NetworkApiUrl.getAccessToken())
                .post(requestBody)
                .build()

        try {

            binding.progressBar.visibility = View.VISIBLE

            val call: Call = okhttp.newCall(request)
            call.enqueue(object : Callback {
                override fun onFailure(call: Call?, e: IOException?) {

                }

                override fun onResponse(call: Call?, response: Response?) {
                    if (response!!.isSuccessful) {

                        responseBodyValue = response.body()?.string()!!
                        val jsonObject = JSONObject(responseBodyValue)
                        val access_token = jsonObject.getString("access_token")

                        /*
                            Only navigate the user inside after the access token is validated in our private server
                         */
                        if (NetworkApi.validateAccessTokenInBackend(access_token, true) != "") {
                            updateUI(account)
                        }

                        Log.d("accessTokenHai1", NetworkApi.validateAccessTokenInBackend(access_token, true))

                    } else {
                        responseBodyValue = "unsuccessful"
                    }

                    launch(UI) {
                        binding.progressBar.visibility = View.GONE
                    }
                }
            })


        } catch (ioe: IOException) {
            Log.e("ioe", ioe.message)
            responseBodyValue = "unsuccessful"
        }

    }

}
