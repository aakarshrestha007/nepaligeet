package com.example.aakarshrestha.mykotlinapplication.UI.adapter


import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.aakarshrestha.mykotlinapplication.BR
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.model.Song
import com.example.aakarshrestha.mykotlinapplication.databinding.ListItemArtistBinding
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_artist.view.*

/**
 * Created by aakarshrestha on 3/2/18.
 */
class RecentlyPlayedTrackAdapter (val context: Context, val listOfSongs: MutableList<Song>) : Adapter<RecentlyPlayedTrackAdapter.BindingHolder>() {

    internal var arrayStr = arrayOf(
            R.drawable.cmcbeats,
            R.drawable.drake,
            R.drawable.postmalone,
            R.drawable.beberexha,
            R.drawable.berrywhite,
            R.drawable.edsheeran,
            R.drawable.postmalone,
            R.drawable.brunomars,
            R.drawable.migos,
            R.drawable.postmalone
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder {
        val itemBinding = DataBindingUtil.inflate<ListItemArtistBinding>(LayoutInflater.from(parent.context), R.layout.list_item_artist, parent, false)
        return BindingHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: BindingHolder, position: Int) {

        val song = listOfSongs[position]
        val itemBinding = holder.binding
        itemBinding.setVariable(BR.msong, song)
        itemBinding.executePendingBindings()

        itemBinding.artistName.text = song.artistname.toString()
        itemBinding.albumName.text = song.albumname.toString()

        try {
            Picasso.with(context).load(arrayStr[position]).into(itemBinding.mImage)
        } catch (e: ArrayIndexOutOfBoundsException) {
            e.printStackTrace()
            Log.e("recentErrorException", e.message)
        }

        itemBinding.root.linear.setOnClickListener {
//            val intent = Intent(context, TrackActivity::class.java)
//            intent.putExtra("r_artistname", itemBinding.artistName.text.toString())
//            intent.putExtra("r_albumname", itemBinding.albumName.text.toString())
//            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return listOfSongs.size
    }


    inner class BindingHolder (val binding: ListItemArtistBinding) : RecyclerView.ViewHolder(binding.root)


}