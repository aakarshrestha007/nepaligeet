package com.example.aakarshrestha.mykotlinapplication.UI.adapter

import android.support.design.widget.CoordinatorLayout
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.databinding.SlideLayoutBinding

/**
 * Created by aakarshrestha on 3/10/18.
 */
class SliderTrackAdapter (val context: Context): PagerAdapter() {

    lateinit var inflater: LayoutInflater
    lateinit var binding: SlideLayoutBinding

    var imageArray = intArrayOf(

            R.drawable.cmcbeats,
            R.drawable.beberexha,
            R.drawable.edsheeran,
            R.drawable.drake,
            R.drawable.brunomars,
            R.drawable.postmalone,
            R.drawable.berrywhite,
            R.drawable.migos

    )

    var artistNameArray = arrayListOf<String>(

            "Cmc Beats",
            "Bebe Rexha",
            "Ed Sheeran",
            "Drake",
            "Bruno Mars",
            "Scary Hours",
            "Berry White",
            "Migos"


    )

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view === `object` as CoordinatorLayout
    }

    override fun getCount(): Int {
        return imageArray.size
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {

        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(inflater, R.layout.slide_layout, container, false)

        binding.centerImage.setImageResource(imageArray[position])
        binding.artistnameInSlider.text = artistNameArray[position]

        container?.addView(binding.root)

        return binding.root
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {

        container?.removeView(`object` as CoordinatorLayout)

    }
}