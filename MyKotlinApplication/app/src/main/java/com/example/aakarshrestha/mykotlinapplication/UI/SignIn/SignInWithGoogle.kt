package com.example.aakarshrestha.mykotlinapplication.UI.SignIn

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.app.FragmentActivity
import android.widget.Toast
import com.example.aakarshrestha.mykotlinapplication.R
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import android.support.annotation.NonNull
import com.example.aakarshrestha.mykotlinapplication.UI.ParentActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException







/**
 * Created by aakarshrestha on 3/24/18.
 */
class SignInWithGoogle {

    companion object signIn {

        fun getGoogleApiClient(context: Context): GoogleApiClient {

             return GoogleApiClient.Builder(context)
                     .enableAutoManage(context as FragmentActivity, {
                         Toast.makeText(context, "Failed to log in with this account. \nPlease try with another account.", Toast.LENGTH_LONG).show()
                     })
                    .addApi(Auth.GOOGLE_SIGN_IN_API, getGoogleSignInOptions(context))
                    .build()
        }

        fun getGoogleSignInOptions(context: Context): GoogleSignInOptions {

            return GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .requestIdToken(context.getString(R.string.server_client_id))
                    .requestServerAuthCode(context.getString(R.string.server_client_id))
                    .build()

        }

        fun getGoogleSignInClient(googleSignInOptions: GoogleSignInOptions, context: Context): GoogleSignInClient {

            return GoogleSignIn.getClient(context, googleSignInOptions)

        }
    }
}