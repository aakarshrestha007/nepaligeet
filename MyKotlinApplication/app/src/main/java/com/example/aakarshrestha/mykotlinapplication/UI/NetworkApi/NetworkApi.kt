package com.example.aakarshrestha.mykotlinapplication.UI.NetworkApi

import android.util.Log
import com.example.aakarshrestha.mykotlinapplication.UI.urls.NetworkApiUrl
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

/**
 * Created by aakarshrestha on 3/2/18.
 */
class NetworkApi {

    companion object {

        val mediaType = MediaType.parse("application/json; charset=utf-8")

        fun getOkhttp(): OkHttpClient {
            return OkHttpClient()
        }

        fun getDataOnCall(murl: String): Call {
            val okhttp = getOkhttp()
            val request = Request.Builder().url(murl).build()
            return okhttp.newCall(request)
        }

        fun postData(email: String, password: String, firstName: String, lastName: String, location: String): String {

            val jsonObject: JSONObject = JSONObject()

            try {

                jsonObject.put("email", email)
                jsonObject.put("password", password)
                jsonObject.put("firstName", firstName)
                jsonObject.put("lastName", lastName)
                jsonObject.put("location", location)

            } catch (je: JSONException) {
                Log.e("je", je.message)
            }

            var responseBodyValue: String

            val okhttp = getOkhttp()
            val requestBodyForm: RequestBody = RequestBody.create(mediaType, jsonObject.toString())

            val request = Request.Builder()
                    .url(NetworkApiUrl.createAccount())
                    .post(requestBodyForm)
                    .build()

            try {

                val response: Response = okhttp.newCall(request).execute()

                if (response.isSuccessful) {

                    responseBodyValue = response.body()?.string()!!

                } else {
                    responseBodyValue = "unsuccessful"
                }


            } catch (ioe: IOException) {
                Log.e("ioe", ioe.message)
                responseBodyValue = "unsuccessful"
            }

            return responseBodyValue
        }

        fun loginToApp(email: String, password: String): String {

            val jsonObject: JSONObject = JSONObject()

            try {

                jsonObject.put("userName", email)
                jsonObject.put("password", password)

            } catch (je: JSONException) {
                Log.e("je", je.message)
            }

            var responseBodyValue: String

            val okhttp = getOkhttp()
            val requestBodyForm: RequestBody = RequestBody.create(mediaType, jsonObject.toString())

            val request = Request.Builder()
                    .url(NetworkApiUrl.loginToAccount())
                    .post(requestBodyForm)
                    .build()

            try {

                val response: Response = okhttp.newCall(request).execute()

                if (response.isSuccessful) {

//                    responseBodyValue = response.body()?.string()!!
                    responseBodyValue = "success"

                } else {
                    responseBodyValue = "unsuccessful"
                }


            } catch (ioe: IOException) {
                Log.e("ioe", ioe.message)
                responseBodyValue = "unsuccessful"
            }

            return responseBodyValue
        }






        fun validateAccessTokenInBackend(accessToken: String, boolVal: Boolean): String {

            val jsonObject: JSONObject = JSONObject()

            try {

                jsonObject.put("accessToken", accessToken)
                jsonObject.put("isNative", boolVal)

            } catch (je: JSONException) {
                Log.e("je", je.message)
            }

            var responseBodyValue: String
            var access_token: String = ""

            val okhttp = NetworkApi.getOkhttp()
            val requestBody: RequestBody = RequestBody.create(NetworkApi.mediaType, jsonObject.toString())

            val request = Request.Builder()
                    .url(NetworkApiUrl.externalAuthGoogle())
                    .post(requestBody)
                    .build()

            try {

                val response: Response = okhttp.newCall(request).execute()
                if (response!!.isSuccessful) {

                    responseBodyValue = response.body()?.string()!!
                    val jsonObject = JSONObject(responseBodyValue)
                    access_token = jsonObject.getString("auth_token")



                    Log.d("accessTokenHai2", access_token)

                } else {
                    responseBodyValue = "unsuccessful"
                }

            } catch (ioe: IOException) {
                Log.e("ioe", ioe.message)
                responseBodyValue = "unsuccessful"
            }

            return access_token

        }

    }

}