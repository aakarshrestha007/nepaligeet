package com.example.aakarshrestha.mykotlinapplication.UI


import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.UI.NetworkApi.NetworkApi
import com.example.aakarshrestha.mykotlinapplication.databinding.FragmentSignUpBinding
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.android.UI
import org.jetbrains.anko.coroutines.experimental.bg
import android.support.design.widget.Snackbar
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import org.jetbrains.anko.sdk15.coroutines.onItemSelectedListener
import java.util.*
import kotlin.collections.ArrayList


/**
 * A simple [Fragment] subclass.
 */
class SignUpFragment : Fragment() {

    lateinit var binding: FragmentSignUpBinding
    var countryname: String = ""

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false)

        getListOfCountries()

        binding.firstname.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.length!! >= 2 && binding.lastname.text.toString().length >= 2 &&
                        binding.email.text.toString().length >= 2 && binding.password.text.toString().length >= 6 &&
                        countryname.isNotEmpty()) {

                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
                    binding.createAccountButton.isEnabled = true


                } else {

                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                    binding.createAccountButton.isEnabled = false

                }

            }
        })

        binding.lastname.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.length!! >= 2 && binding.firstname.text.toString().length >= 2 &&
                        binding.email.text.toString().length >= 2 && binding.password.text.toString().length >= 6 &&
                        countryname.isNotEmpty()) {

                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
                    binding.createAccountButton.isEnabled = true


                } else {

                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                    binding.createAccountButton.isEnabled = false

                }

            }
        })

        binding.email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.length!! >= 2 && binding.lastname.text.toString().length >= 2 &&
                        binding.firstname.text.toString().length >= 2 && binding.password.text.toString().length >= 6 &&
                        countryname.isNotEmpty()) {

                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
                    binding.createAccountButton.isEnabled = true


                } else {

                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                    binding.createAccountButton.isEnabled = false

                }

            }
        })

        binding.password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.length!! >= 6 && binding.lastname.text.toString().length >= 2 &&
                        binding.email.text.toString().length >= 2 && binding.firstname.text.toString().length >= 2 &&
                        countryname.isNotEmpty()) {

                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
                    binding.createAccountButton.isEnabled = true


                } else {

                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                    binding.createAccountButton.isEnabled = false

                }

            }
        })

//        binding.location.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//
//                if (s?.length!! >= 2 && binding.lastname.text.toString().length >= 2 &&
//                        binding.email.text.toString().length >= 2 && binding.password.text.toString().length >= 6 &&
//                        binding.firstname.text.toString().length >= 2) {
//
//                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
//                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
//                    binding.createAccountButton.isEnabled = true
//
//
//                } else {
//
//                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
//                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
//                    binding.createAccountButton.isEnabled = false
//
//                }
//
//            }
//        })

        binding.createAccountButton.setOnClickListener {

            binding.progressbar.visibility = View.VISIBLE
            binding.createAccountButton.visibility = View.GONE

            async(UI) {
                val getData = bg {

                    NetworkApi.postData(binding.email.text.toString().trim(), binding.password.text.toString(), binding.firstname.text.toString().trim(),
                            binding.lastname.text.toString().trim(), /*binding.location.text.toString().trim()*/ "")
                }

                val value = getData.await()

                if (value.trim().equals("Account created", true)) {

                    binding.progressbar.visibility = View.GONE

                    binding.createAccountButton.visibility = View.GONE

                    val intent = Intent(binding.root.context, ParentActivity::class.java)
                    intent.putExtra("firstname", binding.firstname.text.toString().trim())
                    intent.putExtra("lastname", binding.lastname.text.toString().trim())
                    intent.putExtra("email", binding.email.text.toString().trim())
                    /*intent.putExtra("location", binding.location.text.toString().trim())*/
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    binding.root.context.startActivity(intent)

                    activity.finish()

                } else if(value.trim().equals("Validation email sent to user", true)) {

                    binding.confirmationLayout.visibility = View.VISIBLE
                    binding.confirmYourAccount.text = "You are almost there!\nAn email is sent to ${binding.email.text} to confirm your account."
                    binding.signupLayout.visibility = View.GONE

                }

                else {
                    val mySnackbar = Snackbar.make(binding.coordinatorLayout,
                            "${binding.email.text.toString().trim()} already exists!", Snackbar.LENGTH_SHORT)
                    mySnackbar.show()

                    binding.createAccountButton.visibility = View.VISIBLE
                }

                binding.progressbar.visibility = View.GONE
            }

        }

        return binding.root
    }

    fun getListOfCountries() {

        val listOfcountries = arrayListOf<String>()
        listOfcountries.add("Choose country")

        val locales = Locale.getISOCountries()

        locales.forEach { locale ->
            val obj = Locale("", locale)
            listOfcountries.add(obj.displayCountry)
        }

        setUpSpinnerWithListOfCountries(listOfcountries)

    }

    fun setUpSpinnerWithListOfCountries(listOfcountries: List<String>) {

        val arrayAdapter: ArrayAdapter<String> = ArrayAdapter(binding.root.context, android.R.layout.simple_spinner_item, listOfcountries)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.location.adapter = arrayAdapter

        binding.location.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (position > 0) {
                    Log.d("adFree", parent?.getItemAtPosition(position).toString())
                    countryname = parent?.getItemAtPosition(position).toString()

                    if (countryname.isNotEmpty() && binding.firstname.length() >= 2 && binding.lastname.text.toString().length >= 2 &&
                            binding.email.text.toString().length >= 2 && binding.password.text.toString().length >= 6) {

                        binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner)
                        binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, android.R.color.holo_purple))
                        binding.createAccountButton.isEnabled = true

                    } else {
                        binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                        binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                        binding.createAccountButton.isEnabled = false
                    }
                } else {
                    binding.createAccountButton.background = ContextCompat.getDrawable(binding.root.context, R.drawable.rounded_corner_inactive)
                    binding.createAccountButton.setTextColor(ContextCompat.getColor(binding.root.context, R.color.silver))
                    binding.createAccountButton.isEnabled = false
                }

            }

        }

    }

}
