package com.example.aakarshrestha.mykotlinapplication.UI

import android.os.Bundle
import android.app.Fragment
import android.databinding.DataBindingUtil
import android.app.FragmentManager
import android.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.aakarshrestha.mykotlinapplication.R
import com.example.aakarshrestha.mykotlinapplication.databinding.FragmentYourLibraryBinding

class YourLibraryFragment : Fragment() {

    lateinit var binding: FragmentYourLibraryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_your_library, container, false)

        binding.gotoSettings.setOnClickListener({

            val fragment = SettingsFragment()
            val fragmentManager: FragmentManager? = fragmentManager
            val fragmentTransaction: FragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.replace(R.id.container, fragment)?.commit()

        })

        return binding.root
    }
}
